<?php

use App\Http\Controllers\ProductsController;
use App\Http\Controllers\ShopifyController;
use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/products', [ShopifyController::class, 'getProducts']);
Route::get('/products/create/{idProduct}', [ShopifyController::class, 'storeProducts']);
Route::get('/getDataProduct/{id_Shopify}', [ShopifyController::class, 'getProduct']);
Route::get('/deletePublished', [ShopifyController::class, 'deletePublished']);


Route::get('products/dc', [ProductsController::class, 'get_products_dcmayorista']);
