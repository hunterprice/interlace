<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\ShopifyController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Auth::routes(['register' => false]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::get('products', [ProductsController::class, 'index'])->name('products');
ROute::get('logout', [HomeController::class, 'logout'])->name('logout');


ROute::get('pruebas', [ShopifyController::class, 'prueba']);
