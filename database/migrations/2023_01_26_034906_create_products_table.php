<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->integer('idMayorista');
            $table->bigInteger('related');
            $table->integer('selected');
            $table->integer('delisted');
            $table->string('ex_sku');
            $table->string('ex_prod_title');
            $table->string('ex_vendor');
            $table->string('ex_family_id');
            $table->string('ex_family_name');
            $table->string('ex_cat_id');
            $table->string('ex_cat_name');
            $table->string('ex_sub_cat_id');
            $table->string('ex_sub_cat_name');
            $table->string('ex_vendor_id');
            $table->string('ex_prod_barcode');
            $table->string('ex_prod_price');
            $table->string('ex_prod_recently_added');
            $table->string('ex_added_at');
            $table->string('ex_inventory');
            $table->string('shopify_id');
            $table->string('shopify_variants_ids');
            $table->string('icecat_id');
            $table->date('icecat_last_sync');
            $table->string('icecat_ignore_sync');
            $table->float('ex_min_price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
