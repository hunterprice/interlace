<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductsRequest;
use App\Http\Requests\UpdateProductsRequest;
use App\Models\Products;
//use CodeDredd\Soap\Facades\Soap;
use SoapClient;
use RicorocksDigitalAgency\Soap\Facades\Soap;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Products::paginate(10);

        return view('products.list', compact('products'));
    }

    public function get_products_compusoluciones()
    {
        $params = [
            'Usuario' => 'M129812',
            'Psw' => 'THYA1J35',
        ];
        $soap = new SoapClient(null, [
            'location' => 'https://plataformas.siclik.mx/WSB2B/servicio.php',
            'uri' => 'urn:Compusoluciones#GetAll#GetAll',
            'connection_timeout' => 3600,
            'trace' => 1,
            'exceptions' => true,
        ]);
        $data = $soap->__soapCall('GetAll', $params);

        return $data;
    }

    public function get_products_dcmayorista()
    {
        $params = [
            'EncabezadoTransaccion' => [
                'contrasena' => 'RL30688',
                'usuario' => 'Interlace2028',
            ],
        ];
        # Save raw response for debugging
        $soap = new SoapClient(
            'http://dcmayorista.com.mx/ServidorDCWS/CatalogoProdWS?WSDL',
            ['connection_timeout' => 3600, 'trace' => 1, 'exceptions' => true]
        );
        $data = $soap->getCatProduct($params);

        return $data;
    }

    public function get_products_ingrammicro()
    {

    }

    public function get_products_exel()
    {

    }
}
