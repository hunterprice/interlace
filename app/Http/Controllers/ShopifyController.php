<?php

namespace App\Http\Controllers;

use App\Models\Products;
use Illuminate\Http\Request;
use Signifly\Shopify\Shopify;

use Shopify\Rest\Admin2022_10\Product;
use Shopify\Utils;
use Shopify\Context;
use Shopify\Auth\FileSessionStorage;

class ShopifyController extends Controller
{
    public function getProducts()
    {
        // $shopify = app(Shopify::class);

        // $products = $shopify->getProducts();

        $products = Products::where('selected', 1)
            ->limit(1)
            ->get();
        return $products;
    }

    public function storeProducts($idProduct)
    {
        $productData = Products::find(9889);
        $shopify = app(Shopify::class);

        $sku = $productData->ex_sku;
        $icecat_id = $productData->icecat_id;
        $brand = $productData->ex_vendor;
        $ean = trim($productData->ex_prod_barcode);

        $info = $this->fetchProductInfoa($sku, $icecat_id, $brand, $ean);
        $data = $this->getItem($info, 'data');
        $gallery = $this->getItem($data, 'Gallery');
        $general_info = $this->getItem($data, 'GeneralInfo');
        $description = $this->getItem($general_info, 'Description');

        $imgs = [];
        foreach ($gallery as $img) {
            $imgl = [
                'src' => $img->Pic,
            ];
            array_push($imgs, $imgl);
        }

        //crear producto
        $product = $shopify->createProduct([
            'title' => $productData->ex_prod_title,
            'vendor' => $productData->ex_vendor,
            'tags' => [
                $productData->vendor,
                $productData->ex_cat_name,
                $productData->ex_sub_cat_name,
                $productData->ex_family_name,
                'ex_product',
            ],
            'body_html' => $description->LongDesc,
            'metafields' => [
                [
                    'key' => 'brand',
                    'value' => $productData->ex_vendor,
                    'valueType' => 'STRING',
                    'namespace' => 'global',
                ],
                [
                    'key' => 'category',
                    'value' => $productData->ex_cat_name,
                    'valueType' => 'STRING',
                    'namespace' => 'global',
                ],
                [
                    'key' => 'subcategory',
                    'value' => $productData->ex_sub_cat_name,
                    'valueType' => 'STRING',
                    'namespace' => 'global',
                ],
            ],
            'images' => $imgs,
            'product_type' => $productData->ex_cat_name,
        ]);

        //Validar si el min_price es mayor que el ex_prod_price
        if ($productData->ex_min_price > $productData->ex_prod_price) {
            $price = $productData->ex_min_price;
        } else {
            $price = $productData->ex_prod_price;
        }

        //ajuste de la variante sku y precio
        $variant = $shopify->updateVariant($product->variants[0]['id'], [
            'price' => $price,
            'sku' => $productData->ex_sku,
            'barcode' => $productData->ex_sku,
        ]);

        $inventory = $shopify->createInventoryLevel($product->id, [
            'location_id' => 60900638849,
            'inventory_item_id' => 41663060705409,
            'available_adjustment',
            100,
        ]);

        $productData->shopify_id = $product->id;
        $productData->published = 1;
        $productData->slug_shopify = $product->handle;
        $productData->save();

        return $product;
    }

    public function getProduct($id_Shopify)
    {
        $shopify = app(Shopify::class);
        $product = $shopify->getProduct($id_Shopify);
        return $product;
    }

    public function fetchProductInfoa($sku, $icecat_id, $brand, $ean)
    {
        $icecat_user = 'morita-icecat';
        $lang = 'es_mx';
        $cache = 'false';

        $ret = false;
        $code = $icecat_id ?: $ean;
        $urls = [];
        if ($icecat_id) {
            $urls[
                'ID'
            ] = "https://live.icecat.biz/api/?UserName={$icecat_user}&Language={$lang}&icecat_id={$icecat_id}";
        } else {
            if ($ean) {
                $urls[
                    'EAN'
                ] = "https://live.icecat.biz/api/?UserName={$icecat_user}&Language={$lang}&GTIN={$ean}";
            }
            if ($brand && $sku) {
                $brand = urlencode($brand);
                $urls[
                    'SKU'
                ] = "https://live.icecat.biz/api/?UserName={$icecat_user}&Language={$lang}&brand={$brand}&ProductCode={$sku}";
            }
        }
        if ($urls) {
            foreach ($urls as $name => $url) {
                //if ($this->verbose) $this->line("Trying with {$name}...");
                $options = [
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_HEADER => false,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', // who am i
                    CURLOPT_AUTOREFERER => true,
                    CURLOPT_CONNECTTIMEOUT => 120,
                    CURLOPT_TIMEOUT => 120,
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_SSL_VERIFYPEER => true,
                    CURLOPT_SSL_VERIFYHOST => 2,
                    CURLOPT_CAINFO => base_path() . '/cacert.pem',
                ];
                $ch = curl_init($url);
                curl_setopt_array($ch, $options);
                $response = curl_exec($ch);
                if ($response) {
                    $ret = @json_decode($response);
                    if (isset($ret->msg) && $ret->msg == 'OK') {
                        break;
                    } elseif (isset($ret->Code)) {
                        if ($ret->Code == 403) {
                            break;
                        }
                    }
                }
                curl_close($ch);
            }
        }

        return $ret;
    }

    public function get_product_data_aditional($sku, $icecat_id)
    {
        return $data;

        # Get data fields
        $general_info = $this->getItem($data, 'GeneralInfo');
        $image = $this->getItem($data, 'Image');
        $gallery = $this->getItem($data, 'Gallery');
        $features_groups = $this->getItem($data, 'FeaturesGroups');
        $reasons_to_buy = $this->getItem($data, 'ReasonsToBuy');
        $summary_description = $this->getItem(
            $general_info,
            'SummaryDescription'
        );
        $description = $this->getItem($general_info, 'Description');

        $dataF = [
            'descripcion' => $description,
            'images' => $gallery,
            'brand' => $general_info,
        ];

        return $dataF;
    }

    private function getItem($var, $key, $default = '')
    {
        $ret = is_object($var)
            ? (isset($var->$key)
                ? $var->$key
                : $default)
            : (isset($var[$key])
                ? $var[$key]
                : $default);
        return $ret;
    }

    public function prueba()
    {
        //$url ='https://your-development-store.myshopify.com/admin/api/2023-01/products/6549358215297.json';

        $ch = curl_init();

        curl_setopt(
            $ch,
            CURLOPT_URL,
            'https://interlace-mx.myshopify.com/admin/api/2022-10/locations.json'
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        $headers = [];
        $headers[] =
            'X-Shopify-Access-Token: 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';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        $ch = curl_init();

        curl_setopt(
            $ch,
            CURLOPT_URL,
            'https://interlace-mx.myshopify.com/admin/api/2022-10/inventory_levels/adjust.json'
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            "{\"location_id\":60905717889,\"inventory_item_id\":41393602527361,\"available_adjustment\":5}"
        );

        $headers = [];
        $headers[] = 'X-Shopify-Access-Token: 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';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        return $result;
    }
}
