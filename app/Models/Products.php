<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;

    protected $fillable = [
        'idMayorista',
        'related',
        'selected',
        'delisted',
        'ex_sku',
        'ex_prod_title',
        'ex_vendor',
        'ex_family_id',
        'ex_family_name',
        'ex_cat_id',
        'ex_cat_name',
        'ex_sub_cat_id',
        'ex_sub_cat_name',
        'ex_vendor_id',
        'ex_prod_barcode',
        'ex_prod_price',
        'ex_prod_recently_added',
        'ex_added_at',
        'ex_inventory',
        'shopify_id',
        'shopify_variants_ids',
        'icecat_id',
        'icecat_last_sync',
        'icecat_ignore_sync',
        'ex_min_price',
    ];
}
