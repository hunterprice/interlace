@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Productos') }}</div>

                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-9 "></div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <button class="btn btn-success form-control" onclick="ejecutar()">Publicar Productos</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>SKU</th>
                                                <th>Publicado</th>
                                                <th>Producto</th>
                                                <th>Estado Shopify</th>
                                                <th>Vendedor</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @foreach ($products as $product)
                                                <tr>
                                                    <td>{{ $product->ex_sku }}</td>
                                                    <td class="text-center">
                                                        <input class="form-check-input" type="checkbox"
                                                            @if ($product->selected) checked @endif value=""
                                                            id="flexCheckDefault">
                                                    </td>
                                                    <td>{{ $product->ex_prod_title }}</td>
                                                    <td>
                                                        @if ($product->shopify_id != null)
                                                            <strong>SI</strong>
                                                        @else
                                                            No
                                                        @endif
                                                    </td>
                                                    <td>{{ $product->ex_vendor }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="card-footer ">
                        {{ $products->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="ohsnap"></div>
@endsection


@section('js')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <script src="https://code.jquery.com/jquery-3.6.3.min.js"
        integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/1.2.4/axios.min.js"
        integrity="sha512-Th6RhKVKcUO1NdowoioG5HrNgi4JzStsjpwheSR+nWcDIVO4Wv6E6D14o/46EqqDsLSca/rcMD1a3OLyPkexAw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script>
        function ejecutar() {
            axios.get('/api/products').then((data) => {
                toastr.success('Productos Obtenidos con exito')
                const res = data.data

                res.forEach(function(producto, index) {
                    toastr.info('Iniciando Carga de Producto: '+ producto.ex_prod_title)

                    axios.get('/api/products/create/'+producto.id).then((data) => {
                        console.log(data.data)
                        toastr.success('Productos Registrado con exito!')
                    }).catch((e) => {
                        console.log(e)
                        toastr.error('Productos Registrado con exito!', 'No Registrado')
                    })
                })
            })
        }
    </script>
@endsection
