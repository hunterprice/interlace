@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">{{ __('Menu') }}</div>

                    <div class="card-body">
                        <div class="row">
                            <a href="{{ route('products') }}" class="col-md-3 text-center" style="text-decoration: none">
                                    <i class="fa fa-box icon text-success"></i> <br>
                                    <h5 class="mt-3"> <strong>Productos</strong> </h5>
                            </a>

                            <a href="" class="col-md-3 text-center" style="text-decoration: none">
                                    <i class="fa fa-users icon text-info"></i> <br>
                                    <h5 class="mt-3"> <strong>Usuarios</strong> </h5>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
